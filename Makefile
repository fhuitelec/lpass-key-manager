DC_EXEC := docker-compose -f infrastructure/tests/docker-compose.yaml

.PHONY: features
features:
	$(DC_EXEC) build
	$(DC_EXEC) run --rm behave sh -c 'pipenv run behave'

.PHONY: features-debug
features-debug:
	$(DC_EXEC) build
	$(DC_EXEC) run --rm behave sh -c 'pipenv run behave --no-color'

.PHONY: build
build:
	pipenv install --dev
