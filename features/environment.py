from features.steps.utils import run_command


def after_feature(context, feature):
    remove_alpine_dependency('lastpass-cli')


def remove_alpine_dependency(dependency):
    """Removes an alpine dependency using apk"""
    run_command('apk del %s' % dependency)
