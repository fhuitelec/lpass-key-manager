Feature: Check that shell dependencies are installed

    Scenario: It fails when lastpass CLI is not installed
        Given Lastpass CLI is not installed
        When I run the command
        Then the script fails
        And tells us what to do on stderr
