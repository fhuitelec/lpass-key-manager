Feature: List SSH keys


    Scenario: It lists a user SSH keys
        Given I own a LastPass account with a username "someone"
        And I am logged into my LastPass account
        And my SSH key container is "SSH Key"
        And I have the following SSH keys
            | Name            | ID                   |
            | Digital Ocean   | 1877234873309529462  |
            | AWS             | 2737475061829959312  |
            | Some production | 35707844814310100747 |
        When I run the list command
        Then I get the names and IDs of my SSH keys
            | Name            |
            | Digital Ocean   |
            | AWS             |
            | Some production |