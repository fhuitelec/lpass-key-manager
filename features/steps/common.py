import os

from behave import when, given

from features.steps.utils import run_and_capture_command


# Todo: directly pass the command in the features to explicit them <---
# Todo: find a way to set arguments in a nice fashion doing this    ^^^
@when('I run the command')
def step_impl(context):
    username_argument = '' if 'username' not in context else '--username=%s' % context.username
    context.stdout, context.stderr, context.return_code = \
        run_and_capture_command('python lpass_key_manager.py %s' % username_argument)


@when('I run the list command')
def step_impl(context):
    username_argument = '' if 'username' not in context else '--username=%s' % context.username
    context.stdout, context.stderr, context.return_code = \
        run_and_capture_command('python lpass_key_manager.py %s list' % username_argument)


@given('my SSH key container is "{container_name}"')
def step_impl(context, container_name):
    os.environ['LPASS_EXPECTED_CONTAINER'] = container_name
    context.container_name = container_name
