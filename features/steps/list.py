import os

from behave import given, then


@given('I have the following SSH keys')
def step_impl(context):
    ssh_key_list = ''

    for row in context.table:
        ssh_key_list += "%s/%s [id: %s]" % (context.container_name, row['Name'], row['ID']) + '\n'

    os.environ['LPASS_EXPECTED_SSH_LIST'] = ssh_key_list


@then('I get the names and IDs of my SSH keys')
def step_impl(context):
    for row in context.table:
        assert row['Name'] in context.stdout, \
            'SSH key "%s not found in stdout" (rc: "%d", stdout: "%s", stderr: "%s")' \
            % (row['Name'], context.return_code, context.stdout, context.stderr)
