import os

from behave import given, then

from features.steps.utils import run_command


@given('Lastpass CLI is not installed')
def step_impl(context):
    os.environ['LPASS_EXPECTED_IS_INSTALLED'] = 'false'


@given('{dependency} is installed')
def step_impl(context, dependency):
    dependency_mapping = {
        'Lastpass CLI': 'lastpass-cli'
    }

    if dependency not in dependency_mapping:
        raise ValueError('"%s" is not a known dependency' % dependency)

    run_command('apk --update add %s' % dependency_mapping[dependency])


@then('the script fails')
def step_impl(context):
    assert context.return_code == 127,\
        'The command does not fail with a 127 return code (rc: "%d", stdout: "%s")'\
        % (context.return_code, context.stdout)


@then('tells us what to do on stderr')
def step_impl(context):
    assert context.stdout == ''
    assert 'lpass is not installed' in context.stderr,\
        'stderr does not contain the error message("%s" given on stdout)' % context.stderr
