import os

from behave import given, then, when


@given('I own a LastPass account with a username {username}')
def step_impl(context, username):
    os.environ['LPASS_EXPECTED_IS_INSTALLED'] = 'true'
    os.environ['LPASS_EXPECTED_USER'] = username
    context.username = username


@given('I am not logged into my LastPass account')
def step_impl(context):
    os.environ['LPASS_EXPECTED_LOGGED_IN'] = 'false'


@given('I am logged into my LastPass account')
def step_impl(context):
    os.environ['LPASS_EXPECTED_LOGGED_IN'] = 'true'


@when('I enter a correct password')
def step_impl(context):
    """The password management is left to LastPass because it is not
    this tool's responsibility and leaves the security concerns to Lastpass.

    The password management is, thus, not handled in the lpass mock"""
    pass


@then('I am logged in')
def step_impl(context):
    assert 'You have successfully logged into your LastPass account!' in context.stdout, \
        'The user is not logged into their LastPass account (rc: "%d", stdout: "%s")' \
        % (context.return_code, context.stdout)


@then('A message confirms me I am already logged in')
def step_impl(context):
    assert 'You are logged in to LastPass' in context.stdout, \
        'No confirmation message has been displayed (rc: "%d", stdout: "%s")' \
        % (context.return_code, context.stdout)
