import os
from subprocess import Popen, PIPE, call
from typing import Tuple


def run_command(command: str):
    fnull = open(os.devnull, 'w')
    call(command.split(' '), stdout=fnull)


def run_and_capture_command(command: str) -> Tuple[str, str, int]:
    encoding = 'utf-8'
    result = Popen(command.split(' '), stdin=PIPE, stdout=PIPE, stderr=PIPE)
    output, err = result.communicate()

    return output.decode(encoding), err.decode(encoding), result.returncode
