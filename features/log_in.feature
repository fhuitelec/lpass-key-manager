Feature: Log in the user

    Scenario: It asks the user to log into Lastpass
        Given I own a LastPass account with a username "someone"
        And I am not logged into my LastPass account
        When I run the command
        And I enter a correct password
        Then I am logged in

    Scenario: It confirms the user they are already logged into Lastpass
        Given I own a LastPass account with a username "someone"
        And I am logged into my LastPass account
        When I run the command
        Then A message confirms me I am already logged in
