import sys

import click

from utils.command import run_and_capture_command


def check_dependencies():
    _, _, return_code = run_and_capture_command('lpass')

    if return_code == 127:
        click.echo('lpass is not installed', err=True)
        sys.exit(127)


def check_user_is_connected_to_lastpass(username: str):
    _, _, return_code = run_and_capture_command('lpass status')

    if return_code == 0:
        click.echo('You are logged in to LastPass as %s' % username)
        return

    click.echo('You are not connected to LastPass')
    run_and_capture_command('lpass login %s' % username)

    click.echo('You have successfully logged into your LastPass account!')


@click.group(invoke_without_command=True)
@click.option('--username',
              type=str,
              prompt='Enter the username used to log into LastPass',
              help='Username used to log in to lastpass')
def main(username: str):
    check_user_is_connected_to_lastpass(username)


@click.command(name='list')
def list_ssh_keys():
    stdout, stderr, return_code = run_and_capture_command('lpass ls "SSH Key"')

    if return_code > 0:
        raise RuntimeError('Could not retrieve the list of SSH keys: "%s"' % stderr)

    click.echo(stdout)


main.add_command(list_ssh_keys)

if __name__ == '__main__':
    check_dependencies()
    main()
