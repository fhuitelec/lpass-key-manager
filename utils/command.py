from subprocess import Popen, PIPE
from typing import Tuple


def run_and_capture_command(command: str) -> Tuple[str, str, int]:
    encoding = 'utf-8'
    result = Popen(command, stdout=PIPE, stderr=PIPE, shell=True)
    output, err = result.communicate()

    return output.decode(encoding), err.decode(encoding), result.returncode
