#!/usr/bin/env bash

# This commandline is a mock for the lpass CLI tool
# It allows to set expectations and mimic the real lpass command

# Assert installation expectation
[ -z "${LPASS_EXPECTED_IS_INSTALLED}" ] && >&2 echo "Need to set LPASS_EXPECTED_IS_INSTALLED" && exit 1;
[ "${LPASS_EXPECTED_IS_INSTALLED}" != true ] && exit 127;

# Check expectations from environment variables
[ -z "${LPASS_EXPECTED_LOGGED_IN}" ] && >&2 echo "Need to set LPASS_EXPECTED_LOGGED_IN" && exit 1;
[ -z "${LPASS_EXPECTED_USER}" ] && >&2 cho "Need to set LPASS_EXPECTED_USER" && exit 1;

PROGRAM_NAME=$(basename $0)

login () {
    # Check username
    [ -z "${1}" ] && >&2 echo "Need to pass the username argument" && exit 1;
    [ "${LPASS_EXPECTED_USER}" != "${1}" ] \
        && >&2 echo "Username ${1} is not equal to the expected one (${LPASS_EXPECTED_USER})" \
        && exit 1;
}

ls() {
    # Check SSH key list exist
    [ -z "${LPASS_EXPECTED_CONTAINER}" ] && >&2 echo "Need to set LPASS_EXPECTED_CONTAINER" && exit 1;
    [ -z "${LPASS_EXPECTED_SSH_LIST}" ] && >&2 echo "Need to set LPASS_EXPECTED_SSH_LIST" && exit 1;

    [ -z "${1}" ] && echo "Need to pass the container argument" && exit 1;
    [ "${LPASS_EXPECTED_CONTAINER}" != "${1}" ] \
        && >&2 echo "Container ${1} is not equal to the expected one (${1} instead of ${LPASS_EXPECTED_CONTAINER})" \
        && exit 1;

    echo "${LPASS_EXPECTED_SSH_LIST}"
}

status () {
    # Check username
    [ "${LPASS_EXPECTED_LOGGED_IN}" = "false" ] \
        && >&2 echo "Not logged in." \
        && exit 1;
}

sub_command=$1
shift

${sub_command} "${@}"

if [ $? = 127 ]; then
    >&2 echo "Error: '$sub_command' is not a known subcommand." >&2
    exit 1
fi
